""" Python wrapper for the C shared library libeasyframebuff"""
import sys, platform
import ctypes, ctypes.util
  
# Find the library and load it

try:
    easyframebuff = ctypes.CDLL("../lib/libeasyframebuff.so")
except OSError:
    print("Unable to load the system C library")
    sys.exit()

# Make the function names visible at the module level and add types

printframebuff =  easyframebuff.printframebuff
printframebuff.argtypes = [ctypes.c_byte, ctypes.c_char_p]
printframebuff.restype = ctypes.c_int
