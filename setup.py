from setuptools import setup, find_packages
from setuptools.command.build import build

class Build(build):
    """Customized setuptools build command - builds protos on build."""
    def run(self):
        protoc_command = ["make", "python"]
        if subprocess.call(protoc_command) != 0:
            sys.exit(-1)
        build.run(self)



    
setup(
setup_requires=['setuptools_scm'],
use_scm_version=True,
include_package_data=True,
name="pyeasyframebuff",
description="write text to frame buff",
long_description="just a package to write to the oled ssd1306 128*64 pixel display",
url="https://gitlab.com/igcgroup/scale-project/pyeasyframebuff",
author="danny",
author_email="danny@gioxa.com",
cmdclass={
        'build': Build,
},
packages=['pyeasyframebuff']
)