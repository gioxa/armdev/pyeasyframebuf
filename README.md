# Py Easy Framebuff

PyEasyFrame buffer is a python lib to write to a ssd1306 128x64 pixel linux frame buffer.

It allows to write to 4 lines, each 16 characters, with different fonts.


## TOOLS

### Display console

Displays on a console / terminal the bitmap of the screen

```bash
tools/fb2console
```
terminal settings:
```
TERM=term256
FONT_size=5
ANSICOLOR=True
```
Result:

![](oled.png)

### Create bmp from frame buffer

```bash
tools/fb2bmp <bmp-file.bmp>
```
Creates an monochroom(pixel dept of 1) bitmap from the framebuffer and pict_size=128x64

### Create font

Fonts are create as a C source file with an header

```bash
tools/font2c <fontpathname> <output-dir> <Font-Name-Prety>
```

### Building tools

```bash
gcc font2c.c -o font2c
gcc fb2bmp.c -o fb2bmp
gcc fb2console.c -o fb2console
```


## C-lib

building the c-lib:

```bash
make -C lib 
```

## Test

```bash
python3 sample/test_easyframebuff.py
```

### code example Python

```python
from datetime import datetime
import EasyFrameBuff as efb
import ctypes
 
string1 = "This is Danny !!"
string2 = "    BOLD 14     "
string3 = " awesome BOLD16 "

# write to framebuffer
print (efb.printframebuff(0, datetime.now().strftime("%b %d %H:%M:%S ").encode('utf-8')))
print (efb.printframebuff(1,string1.encode('utf-8')))
print (efb.printframebuff(2,string2.encode('utf-8')))
print (efb.printframebuff(3,string3.encode('utf-8')))

```

### Result

![](oled.png)
