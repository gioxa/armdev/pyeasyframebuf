#include <fcntl.h>
#include <unistd.h>
#include "easyframbuff.h"
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include "fonts/Lat7-TerminusBold16.h"
#include "fonts/Lat7-Fixed18.h" 
#include "fonts/Lat7-Terminus16.h"
#include "fonts/Lat7-TerminusBold14.h"

struct linef_s {
    int char_size;
    const char * font;
};

struct linef_s line_font[4];

void init_fonts(void)
{
    line_font[0].char_size=16;
    line_font[0].font=&Lat7_Terminus16[0];

    line_font[1].char_size=18;
    line_font[1].font=&Lat7_Fixed18[0];

    line_font[2].char_size = 14;
    line_font[2].font = &Lat7_TerminusBold14[0];
   
    line_font[3].char_size= 16;
    line_font[3].font=&Lat7_TerminusBold16[0];
}



int  printframebuff(uint8_t line_nr , const char * text)
{
  init_fonts();

  uint8_t framebuffer[16 * 64];
  
  //printf("printframebuff line:%d =>%s<=\n",line_nr,text);

  
   int fbfd = open("/dev/fb0", O_RDONLY) ;

    if (fbfd == -1) {

	printf("Error opening framebuffer %s\n",strerror(errno));

	return(-1);

    }
    /* ToDO check if we can partially read / write, kinda like line by line
    off_t thisoffset=0;
   thisoffset= lseek(fbfd,0,SEEK_SET);
   if (thisoffset== -1) {

	printf("Error lseek start framebuffer %s\n",strerror(errno));
	return(-1);
    }
    printf("fb at offset: %ld\n",thisoffset);
    */
   size_t rr=read(fbfd, &framebuffer, 16 * 64);
   if (rr != (16*64)) {
	    printf("Error reading framebuffer %s\n",strerror(errno));
        if (fbfd != -1) close(fbfd);
	    return(-1);
    }
   if (fbfd != -1) close(fbfd);
    //read standard input and display it
    int position=0;
    int char_size=line_font[line_nr].char_size;
    int offset=0;
    for (int j=0;j<line_nr;j++) offset=offset+ (line_font[j].char_size * 16);

    while (text[position] != 0) 
    {
        int i,put,get;
	    for(i = 0; i < char_size; i++) {

		get = (text[position] * char_size) + i;
		put = (i * 16) + offset + position;

		if(put < (16 * 64)) framebuffer[put] = line_font[line_nr].font[get];

	}
	position++;

	if (position == 16) {

	    break;
	}

    }

    fbfd = open("/dev/fb0", O_WRONLY);

    if (fbfd == -1) {

	printf("Error opening framebuffer 1.\n");
	return(0);

    }
    //this works on both 128x64 abd 128x32 displays
    ssize_t wr=write(fbfd, &framebuffer, 16 * 64);
    if (wr==-1) 
    {
        printf("Error writing framebuffer %s\n",strerror(errno));
        if (fbfd != -1) close(fbfd);
        return -1;
    }
    close(fbfd);

  return 0;
}
