#pragma once

#ifdef __cplusplus
      #include <cstdint>
  #else
      #include <stdint.h>
      #include <stdbool.h>
  #endif

#ifdef __cplusplus
  extern "C" 
  {
#endif
  
  int  printframebuff(uint8_t line_nr , const char * text);
  

#ifdef __cplusplus
  }
#endif