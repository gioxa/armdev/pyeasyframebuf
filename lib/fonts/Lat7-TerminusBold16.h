#ifndef tb_Lat7_TerminusBold16_h
#define tb_Lat7_TerminusBold16_h
#define Lat7_TerminusBold16_len 4096
#define Lat7_TerminusBold16_source "/usr/share/consolefonts/Lat7-TerminusBold16.psf.gz"
extern const char Lat7_TerminusBold16[4096];
#endif
