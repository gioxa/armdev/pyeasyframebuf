init:
	cd lib && make
    pip install -r requirements.txt

test:
    py.test tests

.PHONY: init test