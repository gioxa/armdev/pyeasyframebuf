from datetime import datetime

import EasyFrameBuff as efb
import ctypes
 
print("test print fbuff")

now = datetime.now() # current date and time

date_time = now.strftime("%b %d %H:%M:%S ")

string1 = "This is Danny !!"
string2 = "    BOLD 14     "
string3 = " awesome BOLD16 "
b_string1 = string1.encode('utf-8')

efb.printframebuff(0,date_time.encode('utf-8'))
efb.printframebuff(1,string1.encode('utf-8'))
efb.printframebuff(2,string2.encode('utf-8'))
efb.printframebuff(3,string3.encode('utf-8'))

