unsigned char swapbbits(unsigned char x)
{
    return ( (x & 0x01)<<7 | (x & 0x02)<<5 | (x & 0x04)<<3 |  (x & 0x08)<<1 | (x & 0x80)>>7 | (x & 0x40)>>5| (x & 0x20)>>3| (x & 0x10)>>1 );
}

void readfbbmp()
{
    uint8_t framebuffer[16 * 64];
    int fbfd = open("/dev/fb0", O_RDONLY) ;

    if (fbfd == -1) {

	printf("Error opening framebuffer %s\n",strerror(errno));

	return(-1);

    }
  
    size_t rr=read(fbfd, &framebuffer, 16 * 64);
    if (rr != (16*64)) 
    {
        printf("Error reading framebuffer %s\n",strerror(errno));
        if (fbfd != -1) close(fbfd);
	    return(-1);
    }
    if (fbfd != -1) close(fbfd);

    int fbmp = open(argv[1], O_WRONLY | O_CREAT | O_EXCL, 0666) ;
    if ((fbmp == -1) && (EEXIST == errno))
    {
        /* open the existing file with write flag */
        fbmp = open(argv[1], O_WRONLY);
    }
    if (fbmp == -1) {

	printf("Error opening bmp file %s\n",strerror(errno));

	return(-1);

    }
    char file_type_data[]=
    {
        0x42,0x4D,     /* file ident bmp */
        0x3e,0x04,0,0, /* file size */
        0,0,           /* reserved */
        0,0,           /* reserved */
        0x3e,0,0,0     /* offset to pixel data */
    }; // 14
    char image_information_data[]=
    {
        0x28,0,0,0,  /* Header size */
        0x80,0,0,0,  /* bmp size x */
        0x40,0,0,0,  /* bmp size y */
        1,0,         /* planes */
        1,0,         /* bits/pixel */
        0,0,0,0,     /* value of Compresion 0 if no compression */
        0,0,0,0,     /* Image Size after compresion 0 if no compresion */
        0,0,0,0,     /* X pixel per meter 0 if no preference */
        0,0,0,0,     /* Y pixel per meter 0 if no preference */
        0,0,0,0,     /* total Colors if 0, 2^bits/pixel is used*/  
        0,0,0,0      /* important colors, generaly ignored, set to 0*/
    }; // 40
    char color_pallet_data[]=
    {
        0xff,0xff,0xff,0,0,0,0,0
    }; // 8
    write(fbmp,&file_type_data,14);
    write(fbmp,&image_information_data,40);
    write(fbmp,&color_pallet_data,8);


    // Flip vertical
    for (int i=0;i<64;i++)
    {
        for (int j=0;j<16;j++)
        { 
            unsigned char b=swapbbits(framebuffer[(63-i)*16+j]);
            write(fbmp,&b,1);
        }
    }
    close(fbmp);

    return 0;
}
*/