/* Program to render text on SSD1306 framebuffer at /dev/fb1
 *
 * Copyright (c) 2019 mincepi <mincepi@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * To compile run: gcc -lz -o text2fb1 text2fb1.c
 *
 * To use, start the program, specifying a console font file, then
 * send text to it's standard input.
 *
 * No control character handling is performed.
 *
 * Only PSF1 fonts with heights of 32 lines or less are supported.
 *
 * Only the first 256 characters of the font are used.
 *
 * Every 16 characters the text wraps to the next line of the display.
 *
 * Example: echo -n "Hello           World" | ./text2fb1 /usr/share/consolefonts/Lat7-Terminus16.psf.gz
 * will display Hello on the first line, World on the second.
 *
 */

#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <zlib.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>

int charsize, get, put, i, j;
int position = 0;
int line = 0;
gzFile fontfd;
char font[8192];
char buf;
uint8_t framebuffer[16 * 64];
int  file_size;
	FILE *out,*out_h;

static FILE *open_file ( char *file, char *mode )
{
	FILE *fp = fopen ( file, mode );
	
	if ( fp == NULL ) {
		perror ( "Unable to open file" );
		exit ( -1 );
	}
	
	return fp;
}

int main(int argc, char **argv) {
 

	if ( argc != 4 ) {
		fprintf ( stderr, "Usage: %s <fontfile> <output_path> <name>\n", argv[0] );
		exit ( EXIT_FAILURE );
	}
	else
		printf("reading: %s\r\n...\r\n",argv[1]);

	ssize_t len=strlen(argv[2]) + strlen(argv[3]);
	
	char *out_name_c=calloc(1, len+6);
	char *out_name_h=calloc(1, len+6);
	
	sprintf(out_name_c, "%s/%s.c",argv[2],argv[3]);
	sprintf(out_name_h, "%s/%s.h",argv[2],argv[3]);
	

	fontfd = gzopen(argv[1], "rb");

	if (fontfd == Z_NULL) {

	printf("Error opening font file or file doesn't exist.\n");
	return(0);

	}

	gzread(fontfd, &font, 4);

	if ((font[0] != 0x36) | (font[1] != 0x04)) {

	printf("Font is not a PSF1 font.\n");
	gzclose(fontfd);
	return(0);

	}

	if (font[3] > 32) {

	printf("Font is to large.\n");
	gzclose(fontfd);
	return(0);

	}

	charsize = font[3];
     file_size=charsize * 256;
	gzread(fontfd, &font, file_size);
	gzclose(fontfd);

	//reverse bits
	for( i = 0; i < (charsize * 256); i++) {

	get = font[i];
	put = 0;

	for( j = 0; j < 8; j++) {

		if ((get & (1 << j)) > 0) put |= (0x80 >> j);

	}

	font[i] = put;

	}

	out = open_file ( out_name_c, "w" );
	out_h = open_file ( out_name_h, "w" );
	
	char * clean_name=calloc(1,strlen(argv[3])+1);
	sprintf(clean_name,"%s",argv[3]);
	int  i=0;
	for (i=0;i<strlen(argv[3]);i++)
	{
		if (clean_name[i]=='-' || clean_name[i]=='.') clean_name[i]='_';
	}
	
	fprintf(out_h,"#ifndef tb_%s_h\n",clean_name);
	fprintf(out_h,"#define tb_%s_h\n",clean_name);
	
	fprintf(out_h,"#define %s_len %zd\n",clean_name,file_size);
	fprintf(out_h,"#define %s_source \"%s\"\n",clean_name,argv[1]);
	fprintf(out_h,"extern const char %s[%zd];\n",clean_name,file_size);
	fprintf(out_h,"#endif\n");
	
	fprintf(out,"const char %s[%zd] = \n\t{\n\t",clean_name,file_size);

    ssize_t thiscount=0;
	while ( thiscount < file_size)
	{
		fprintf(out,"0x%02x",font[thiscount]);
		if (thiscount < file_size -1)
			fprintf(out,", ");
		else
			fprintf(out,"\n\t");
		thiscount++;
		if (! (thiscount%32)  ) fprintf(out,"\n\t");
	}
	fprintf(out,"};");
	fclose ( out );
	fclose(out_h);
	printf("wrote: %s/%s.c \n",argv[2],argv[3]);
	printf("wrote: %s/%s.h",argv[2],argv[3]);
	printf(" Content lengt : %zd\n",file_size);
	return(0);
}
