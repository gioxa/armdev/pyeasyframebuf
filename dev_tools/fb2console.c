#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdint.h>
#include "ansicolor.h"

unsigned char swapNibbles(unsigned char x)
{
    return ( (x & 0x0F)<<4 | (x & 0xF0)>>4 );
}


unsigned char swapbbits(unsigned char x)
{
    return ( (x & 0x01)<<7 | (x & 0x02)<<5 | (x & 0x04)<<3 |  (x & 0x08)<<1 | (x & 0x80)>>7 | (x & 0x40)>>5| (x & 0x20)>>3| (x & 0x10)>>1 );
}

int main(int argc, char **argv) 
{
    uint8_t framebuffer[16 * 64];
    int fbfd = open("/dev/fb0", O_RDONLY) ;

    if (fbfd == -1) {

	printf("Error opening framebuffer %s\n",strerror(errno));

	return(-1);

    }
  
    size_t rr=read(fbfd, &framebuffer, 16 * 64);
    if (rr != (16*64)) 
    {
        printf("Error reading framebuffer %s\n",strerror(errno));
        if (fbfd != -1) close(fbfd);
	    return(-1);
    }
    if (fbfd != -1) close(fbfd);
    printf(YEL);
    // Flip vertical
    for (int i=0;i<64;i++)
    {
        if (i==16) printf(BLU);
        for (int j=0;j<16;j++)
        {
            char b=framebuffer[i*16+j];
            for (int x=0;x<8;x++)
            {
                if (b & (1 << x))
                    printf("\u2588");
                else 
                    printf (" ");
            }
        }
        printf("\n");
    }
    printf(reset);

    return 0;
}